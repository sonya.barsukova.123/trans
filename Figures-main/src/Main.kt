fun main() {

    val f = Rect(4,3,2,1)

    println("Rotation Rect(" + f.c_pos() +") вокруг (x:3,y:-3) по часовой: ")
    f.rotate(RotateDirection.Clockwise,3,-3)
    println(f.c_pos())

    val f1 = Square(4,3,2)

    println("Rotation Square(" + f1.c_pos() + ") вокруг (x:3,y:-3) против часовой: ")
    f1.rotate(RotateDirection.CounterClockwise,3,-3)
    println(f1.c_pos())

    println("Resize x2, Rect S = " + f1.area())
    f1.resize(2)
    println(f1.area())

    val f2 = Circle(2,2,3)

    println("Rotation Circle(" + f2.c_pos() + ") вокруг (x:0,y:0) по часовой: ")
    f2.rotate(RotateDirection.Clockwise,0,0)
    println(f2.c_pos())

    println("Resize x2, Circle S= " + f2.area())
    f2.resize(2)
    println(f2.area())

    println("Move(2,2) Circle(" + f2.c_pos() + "):")
    f2.move(2, 2)
    println(f2.c_pos())

}